# Telegram Instant View

Aqui estará um banco de códigos rhashs de templates criados para o Telegram Instant View. Com esses códigos, você poderá em seus bots para a conversão manual em sites que ainda não foram aprovados.


## Avisos importantes
A intenção do repositório é que facilite a usabilidade dos bots que fazem a conversão de sites comuns para Instant View. Com este repositório, o dev poderá criar bots que irão verificar se o link enviado no grupo contém aqui e caso contenha, fazer a conversão para o IV de forma mais simples.

- As urls não conterão o `https` já que alguns dos sites listados utilizam de subdominios, como por exemplo, o Hasnode.dev, que os nomes de usuários estão como subdominios (`username.hashnode.dev`), então para facilitar a integração em bots, não será acrescentado o `https` para que assim, possam fazer uma integração e o bot faça uma leitura mais precisa.

## API
`title`: O título do site

`domain`: Caso o site com template tenha subdimonios diferentes para o mesmo blog, por motivos diversos. Nesse caso, o template estará ligado ao dominio e não ao link de origem

> Exemplo:
>
> `user1.hasnode.dev/artigo`
>
> `user2.hasnode.dev/artigo`
> domain: `hasnode.dev`

`origin`: link de origem. Aquele que vem após o `https://` e antes do barra (`/`)
> Exemplo:
>
> `https://freecodecamp.org/news/artigo`
>
> origin: `freecodecamp.org`

`rhash`: código usado para o telegram para achar o template.

## Uso do template manual

Enquanto o Telegram não aprova o template, ou não pode, há a possibilidade de ativar o modo de leitura rápida usando o padrão de link a seguir:

> `https://t.me/iv?url=<link_do_artigo>&rhash=<código_rhash>`

Dessa forma, o bot do instant view irá achar o template referente ao link e irá habilitar o modo de leitura rápida para o link enviado.
